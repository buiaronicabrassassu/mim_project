import os.path
import shutil

PHOTOS_FOLDER = "photos"
SRC_PHOTOS_PATH = PHOTOS_FOLDER+"/PisaDataset/JPG"
PISA_DATASET_FILE_URL = os.path.realpath(PHOTOS_FOLDER+"/dataset_pisa.txt")

def getFileClassFolderPath(fileClass):
    return os.path.realpath(PHOTOS_FOLDER+"/"+fileClass.lower())

def getSrcFilePath(fileName):
    return os.path.realpath(SRC_PHOTOS_PATH+"/"+fileName+".jpg")

def getDestFilePath(fileName, fileClass):
    fileClassPath=getFileClassFolderPath(fileClass)
    return os.path.realpath(fileClassPath+"/"+fileName+".jpg")


def readDatasetFile(datasetFileURL):
    try:
        pisaDatasetFile = open(datasetFileURL,"r")
        fileList = pisaDatasetFile.readlines()

        return fileList

    except  IOError:
        print("ERROR: "+PISA_DATASET_FILE_URL+" not found")
    else:
        pisaDatasetFile.close()
        return []


def copyFiles(fileList):
    for file in fileList:
        [fileName, fileClass] = file.split("\t", 1)
        fileClassPath = getFileClassFolderPath(fileClass)

        if(not os.path.exists(fileClassPath)):
            os.makedirs(fileClassPath)
            print(fileClassPath)

        srcFilePath = getSrcFilePath(fileName)
        if(os.path.exists(srcFilePath)):
            shutil.copy(srcFilePath, fileClassPath)

def isCopySuccesfullyCompleted(fileList):
    for file in fileList:
        [fileName, fileClass] = file.split("\t", 1)
        if(not os.path.exists(getDestFilePath(fileName,fileClass))):
            return False
    return True
    


fileList = readDatasetFile(PISA_DATASET_FILE_URL)
copyFiles(fileList)
if(isCopySuccesfullyCompleted(fileList)):
    print("copy completed")
else:
    print("copy faild")

